FROM alpine:3.14

LABEL maintainer='r0b3rt'

RUN apk update \
    && apk add --no-cache mysql-client \
    && apk add python3 \
    && apk add apache2 \
    && apk add vim \
    && apk add nodejs

COPY index.html /var/www/localhost/htdocs/

RUN apk update

EXPOSE 80/tcp

